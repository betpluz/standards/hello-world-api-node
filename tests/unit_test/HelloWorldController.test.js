const Request = require('supertest');
const app = require('../../hello_world_api');

describe('Resquet of the route GET method', () => {
  beforeAll(() => {
    app.close();
  });

  test('Should answer a status 200', () => {
    Request(app).get('/')
      .then((res) => {
        expect(res.statusCode).toBe(200);
        expect(res.statusCode).not.toBe(404);
      });
  });

  test('Should return a string', () => {
    Request(app).get('/')
      .then((res) => {
        expect(res.text).toBe('Hello World API...!');
        expect(typeof res.text).toBe('string');
        expect(typeof res.text).not.toBe('undefined');
      });
  });
});

describe('Resquet of the route not Valid', () => {
  beforeAll(() => {
    app.close();
  });

  test('Should return route Invalid of GET method', () => {
    Request(app).get('/HelloWorld')
      .then((res) => {
        expect(res.statusCode).toBe(404);
        expect(res.statusCode).not.toBe(200);
        expect(res.text).toBe('Invalid Route not Exist');
        expect(typeof res.text).toBe('string');
        expect(typeof res.text).not.toBe('undefined');
      });
  });

  test('Should return route Invalid of POST method', () => {
    Request(app).post('/HelloWorld')
      .then((res) => {
        expect(res.statusCode).toBe(404);
        expect(res.statusCode).not.toBe(200);
        expect(res.text).toBe('Invalid Route not Exist');
        expect(typeof res.text).toBe('string');
        expect(typeof res.text).not.toBe('undefined');
      });
  });
});
