# Hello World API

## Prerequisites
Before installing, [download and install Node.js](https://nodejs.org/en/download/). Node.js 0.10 or higher is required.

## Install and Usage
  To use the API, you must first clone the repository hello-world-api-node and install the dependencies

```bash
$ git clone https://gitlab.com/betpluz/standards/hello-world-api-node.git
$ cd hello-world-api-node
$ npm install
$ npm start
```

## Tests

### Unit Test
  To run the test suite, first install the dependencies, then run `npm test`:

```bash
$ npm install
$ npm test
```

### Tests with Eslint
  To run tests with Eslint, first install the dependencies, then run `npm run eslintest`:

  ```bash
  $ npm install
  $ npm run eslintest
  ```
