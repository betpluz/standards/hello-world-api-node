const appName = 'helloWorldController';
const Mydebbuger = require('debug')(appName);

const getHelloWorld = (req, res) => {
  const Sentry = req.app.get('sentry');
  try {
    const io = req.app.get('socketio');
    const myResponse = 'Hello World API...!';
    io.emit('HELLO', myResponse);
    res.json({ result: myResponse });
  } catch (e) {
    Mydebbuger('Error caught in getHelloWorld: ', e);
    Sentry.captureException(e);
  }
};

const getNotRouteValid = (req, res) => {
  const Sentry = req.app.get('sentry');
  try {
    const myResponse = 'Invalid Route not Exist';
    res.status(404).send(myResponse);
  } catch (e) {
    Mydebbuger('Error caught in getNotRouteValid: ', e);
    Sentry.captureException(e);
  }
};

module.exports = {
  getHelloWorld,
  getNotRouteValid,
};
