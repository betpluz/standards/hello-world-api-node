const helloWorldController = require('../controllers/HelloWorldController');

module.exports = (router) => {
  router.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });


  // routes

  router.get('/', helloWorldController.getHelloWorld);

  router.get('*', helloWorldController.getNotRouteValid);

  router.post('*', helloWorldController.getNotRouteValid);
};
