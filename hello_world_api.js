const appName = 'hello_world_api';
require('dotenv').config();
const app = require('express')();
const bodyParser = require('body-parser');
const Mydebbuger = require('debug')(appName);
const Sentry = require('@sentry/node');
const socketIO = require('socket.io');

const sentryNode = process.env.SENTRY_NODE;
Sentry.init({
  dsn: sentryNode,
});

app.set('sentry', Sentry);

const port = process.env.SRVC_HELLO_WORLD_API_PORT
|| process.env.SETUP_HELLO_WORLD_API_PORT; // obtain the PORT for the service

const routes = require('./routes/routes');

// midlewares
app.use((req, res, next) => {
  Mydebbuger('App is in use, got called with this URL: ', req.url, ' and request method', req.method);
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Request-Method', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(Sentry.Handlers.requestHandler());

// start the server
const server = app.listen(port, (err) => {
  if (err) {
    Mydebbuger('Error at server creation: ', err);
    Sentry.captureMessage(err);
    process.exit(1);
  } else {
    Mydebbuger('Server listening on port: ', port);
  }
});
module.exports = server;

// connect to socket.IO
const io = socketIO(server);

io.on('connection', (socket) => {
  Mydebbuger('new connection: ', socket.id);
  app.set('socketio', socket);
});

// routes
try {
  routes(app);
  app.use(Sentry.Handlers.errorHandler());
} catch (e) {
  Mydebbuger('error in the creation of server: ', e);
  Sentry.captureException(e);
}
